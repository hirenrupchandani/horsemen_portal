<!DOCTYPE html>
<html>
<head>
<title>USA UNIVERSITIES</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<style type="text/css">
body{
                margin: 0;
                padding: 0;
                background-size: cover;
                font-family: calibri;
                font-weight: bold;
                font-size: 27px;
                background-color: #00003A;
            }
            
.logo{
                width: 100%;
                height: 30px;
                position: absolute;
                top: -50px;
                left: calc(50% - 50px);
    }
    
.signinbox{
                width:80%;
                height: 752px;
                background: #F5EC83;
                color: #000;
                top: 100%;
                left: 50%;
                position: absolute;
                transform: translate(-50%,-50%);
                box-sizing: border-box;
                border: solid 2px black;
                padding: 30px 30px;
            }

    h1{
                margin: 0;
                padding: 0 0 20px;
                text-align: left;
                font-size: 20px;
            }
            
            .signinbox p{
                margin: 0;
                padding: 0;
                font-weight: bold;
            }
            
            .signinbox input{
                width: 100%;
                margin-bottom: 5px;
            }
            
            .signinbox input[type="text"], input[type="password"]{
                border: none;
                border-bottom: 1px solid #000;
                background: transparent;
                outline: none;
                height: 40px;
                font-size: 16px;
            }
            
            
            .signinbox input[type="submit"]{
                width: 30%;
                border: solid 2px black;
                outline: none;
                height: 40px;
                background:#9B0204;
                color: #fff;
                font-size: 18px;
                border-top-left-radius: 20px;
                border-bottom-right-radius: 20px;
            }
            
            .signinbox label{
                padding:0px;
            }
            
            
            .signinbox input[type="checkbox"]{
                cursor: pointer;
                color: #fff;
                border: solid 2px black;
                height: 20px;
                width:auto;
            }

    
    
            .signinbox input[type="submit"]:hover{
                cursor: pointer;
                background-color:red; 
                color: #fff;
                border: solid 2px black;
            }
            
            .signinbox a{
                text-decoration: none;
                font-size: 14px;
                line-height: 40px;
                color: crimson;
                
            }
            
            .signinbox a:hover{
                color: darkturquoise;
            }
            
            .signinbox input[type="button"]{
                
                width: 100%;
                border: none;
                outline: none;
                height: 30px;
                background: #FF005A;
                color: #fff;
                font-size: 16px;
                border-radius: 25px;
            }

input[type=text]:focus {
    border: 3px solid black;
}

    .signinbox input[type="CHECKBOX"]{
                border: none;
                border-bottom: 1px solid #000;
                background: transparent;
                outline: none;
                height: 20px;
                font-size: 16px;
            }
        
</style>
    
<script type="text/javascript" language="JavaScript">
	var reg;
	function validate()
	{
	   if(!validateName() | !validateCgpa() | !validateGre() | !validateBudget())
       {
           alert("Error!");
           return false;
       }
        else
			return true;	
    }
    
    function validateName()
			{
				name = document.getElementById("name").value;
				reg = /^[A-Za-z]+\s[A-Za-z]+$/;
				
				if(name=="")
				{	
					document.getElementById("nameError").innerHTML="Please enter your name";
					return false;
				}
				else if(!reg.test(name))
				{
					document.getElementById("nameError").innerHTML="Please enter a valid name";
					return false;
				}
				else
				{
					document.getElementById("nameError").innerHTML="";
					return true;
				}
			}
    
    function validateCgpa()
			{
				cgpa = document.getElementById("cgpa").value;
				cgpavalue = parseFloat(document.getElementById("cgpa").value)
                reg = /^[0-9][0-9]?.?[0-9][0-9]?$/;
				
				if(cgpa=="")
				{
					document.getElementById("cgpaError").innerHTML="Please enter your CGPA score";
					return false;
				}
				else if(!reg.test(cgpa))
				{
					document.getElementById("cgpaError").innerHTML="Invalid CGPA";
					return false;
				}
                else if(cgpavalue<0 | cgpavalue>10)
                {
                    document.getElementById("cgpaError").innerHTML="Invalid CGPA";
					return false;
                }
				else
				{
					document.getElementById("cgpaError").innerHTML="";
					return true;
				}
			}
    
    function validateGre()
			{
				gre = document.getElementById("gre").value;
				grevalue = parseInt(document.getElementById("gre").value);
                reg = /^[0-9]+$/;
				
				if(gre=="")
				{
					document.getElementById("greError").innerHTML="Please enter your GRE score";
					return false;
				}
				else if(!reg.test(gre))
				{
					document.getElementById("greError").innerHTML="Invalid GRE score";
					return false;
				}	
				else if(grevalue>340)
                {
                    document.getElementById("greError").innerHTML="Invalid GRE score";
					return false;
                }
                else
				{
					document.getElementById("greError").innerHTML="";
					return true;
				}
			}
    
    function validateBudget()
			{
				budget = document.getElementById("budget").value;
				reg = /^[0-9]+$/;
				
				if(budget=="")
				{
					document.getElementById("budgetError").innerHTML="Please enter your Budget";
					return false;
				}
				else if(!reg.test(budget))
				{
					document.getElementById("budgetError").innerHTML="Invalid value";
					return false;
				}	
				else
				{
					document.getElementById("budgetError").innerHTML="";
					return true;
				}
			}
</script>  
    
</head>
<body style="text-align:left;color:black";>
<div class="container-fluid">
    <img src="Masters-in-the-USA-1.jpg" height="400px" width="1320px">
</div>
<div class="signinbox">
    
<legend><b><center><h3>ENTER THE NECESSARY DETAILS</h3></center></b></legend>
    <form name="myform" action="getform.php" onsubmit="return validate()" method="get">
        NAME:<input type="text" placeholder="USERNAME/FULL NAME" id="name" name="name" onchange="return validateName()">&nbsp;&nbsp;<label id="nameError" style="color:red;font-size:65%;"></label><br>
        CGPA:<input type="text" id="cgpa" name="cgpa" onchange="return validateCgpa()"><br>&nbsp;&nbsp;<label id="cgpaError" style="font-size:65%;color:red;"></label><br>
        GRE:<input type="text" id="gre" name="gre1" onchange="return validateGre()"><br>&nbsp;&nbsp;<label id="greError" style="color:red;font-size:65%;"></label><br>
        REGION:&nbsp &nbsp<select name="region" name="region">
        <option value="ARIZONA">ARIZONA</option>
        <option value="CALIFORNIA">CALIFORNIA</option>
        <option value="COLORADO">COLORADO</option>
        <option value="FLORIDA">FLORIDA</option>
        <option value="ILLINOIS">ILLINOIS</option>
        <option value="INDIANA">INDIANA</option>
        <option value="MASSACHUSETTS">MASSACHUSETTS</option>
        <option value="NEW JERSEY">NEW JERSEY</option>
        <option value="NORTH CAROLINA">NORTH CAROLINA</option>
        <option value="PENNSYLVANIA">PENNSYLVANIA</option>
        </select>
        <br><br>
        BUDGET:<input type="text" id="budget" placeholder="$" name="budget" onchange="validateBudget()"><br>&nbsp;&nbsp;<label id="budgetError" style="color:red;font-size:65%;"></label><br>
        <input type="checkbox" checked="checked" name="TOP 10" value="TOP 10">&nbsp;TOP 10
        &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" checked="checked" name="REGION" value="REGION">&nbsp;REGION
        <br>
        <center>
        <input type="submit" value="SUBMIT" name="SUBMIT"/>
        </center>
        <br>
        </fieldset>
    </form>
</div>
</body>
</html>